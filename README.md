# Merlin-rs

Merlin-rs is a cross-platform post-exploitation Command & Control server and agent written in Rust. It is based off of and intended to be compatible with Merlin, which is written in Go

## Quick Start

1. Download the latest compiled version of Merlin-rs server
2. Extract the files
3. Start merlin
4. Confugre a listener
5. Deploy an agent

## Features

Feature parity with Merlin is a far off goal.

- [x] Shell
- [ ] Donut integration
- [ ] HTTP/3 Support with Quiche
- [ ] Banner
- [ ] JOSE with biscuit
- [ ] OPAQUE with opaque-ke
- [ ] sRDI integration
- [ ] Agent Code
- [ ] Possible Mythic integration
- [ ] Traffic padding
- [ ] SharpGen integration
- [ ] Documentation
- [ ] http/1.1 clear-text support
- [ ] http/1.1 over TLS support
- [ ] HTTP/2 support
- [ ] HTTP/2 clear-text support
- [ ] Multi-Platform support (Only linux has been tested)
- [ ] Domain Fronting

## Minimum Supported Rust Version
rustc 1.59
