// Standard
use std::error::Error;
use std::fmt;

// 3rd Party
use owo_colors::OwoColorize;
use shellfish::{Command, Shell};

// Modules

const H2C: &str = "h2c";
const HTTP: &str = "http";
const HTTPS: &str = "https";
const HTTP2: &str = "http2";
const HTTP3: &str = "http3";

#[async_std::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let prompt = format!(
        "{}{}{}",
        "Merlin-rs[".red(),
        "listeners".green(),
        "]»".red()
    );
    let mut shell = Shell::new_async(0_u64, prompt);

    shell.commands.insert(
        "configure",
        Command::new(
            "Interact with and configure a named listener.".to_string(),
            configure,
        ),
    );
    shell.commands.insert(
        "delete",
        Command::new("Delete a named listener.".to_string(), delete),
    );
    shell.commands.insert(
        "info",
        Command::new(
            "Display all information about a listener.".to_string(),
            info,
        ),
    );
    shell.commands.insert(
        "list",
        Command::new("List all created listeners.".to_string(), list),
    );
    shell.commands.insert(
        "start",
        Command::new("Start a named listener.".to_string(), start),
    );
    shell.commands.insert(
        "stop",
        Command::new("Stop a named listener.".to_string(), stop),
    );
    shell.commands.insert(
        "use",
        Command::new(
            "Create a new listener by protocol type.".to_string(),
            create,
        ),
    );

    shell.run_async().await?;
    Ok(())
}

fn configure(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn delete(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn info(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn list(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn start(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn stop(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn create(_state: &mut u64, args: Vec<String>) -> Result<(), Box<dyn Error>> {
    let arg = args.get(1).ok_or_else(|| Box::new(ArgError))?;
    match arg {
        a if a == H2C => todo!(),
        b if b == HTTP => todo!(),
        c if c == HTTPS => todo!(),
        d if d == HTTP2 => todo!(),
        e if e == HTTP3 => todo!(),
        _ => println!("Unkown Protocol"),
    };
    Ok(())
}

#[derive(Debug)]
pub struct ArgError;

impl fmt::Display for ArgError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "No argument given")
    }
}

impl Error for ArgError {}
