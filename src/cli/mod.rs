// Standard
use std::error::Error;

// 3rd Party
use owo_colors::OwoColorize;
use shellfish::{Command, Shell};

// Modules
pub mod banner;
pub mod menus;

#[async_std::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let mut shell = Shell::new_async(0_u64, "Merlin-rs» ".red());

    shell.commands.insert(
        "banner",
        Command::new("prints the banner.".to_string(), printbanner),
    );
    shell.commands.insert(
        "version",
        Command::new("prints the version.".to_string(), version),
    );
    shell.commands.insert(
        "agent",
        Command::new("Interact or list agents.".to_string(), agent),
    );
    shell.commands.insert(
        "clear",
        Command::new("Clears all unset jobs.".to_string(), clear),
    );
    shell.commands.insert(
        "group",
        Command::new("Add, remove, or list groups.".to_string(), group),
    );
    shell.commands.insert(
        "interact",
        Command::new("Interact with an agent.".to_string(), interact),
    );
    shell.commands.insert(
        "listeners",
        Command::new("Move to the listeners".to_string(), listeners),
    );
    shell.commands.insert(
        "remove",
        Command::new("Remove or delete a DEAD agent.".to_string(), remove),
    );
    shell.commands.insert(
        "sessions",
        Command::new(
            "Display a table of information abour agent sessions.".to_string(),
            sessions,
        ),
    );
    shell.commands.insert(
        "jobs",
        Command::new("Display all unfinished jobs.".to_string(), jobs),
    );
    shell.commands.insert(
        "use",
        Command::new("Use a Merlin-rs module".to_string(), merlin_use),
    );

    shell.run_async().await?;

    Ok(())
}

fn version(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn printbanner(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn merlin_use(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn jobs(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn agent(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn clear(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn group(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn interact(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn listeners(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    menus::listen_menu::main().expect("Listen Menu failed");
    Ok(())
}

fn remove(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}

fn sessions(_state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("placeholder");
    Ok(())
}
