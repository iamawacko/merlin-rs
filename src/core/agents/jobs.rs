// Standard

// 3rd Party
use uuid::Uuid;

struct info {
    AgentID: Uuid,
    Type: String,
    Token: Uuid,
    Status: i32,
    Chunk: i32,
    Created: std::time::Duration,
    Sent: std::time::Duration,
    Completed: std::time::Duration,
    Command: String,
}

fn Add() {
    todo!();
}

fn ClearCreated() -> Result<(), std::error::Error> {
    todo!();
}

fn Get(AgentID: Uuid) -> Result<String, std::error::Error> {
    todo!();
}

fn Handler() -> Result<String, std::error::Error> {
    todo!();
}

fn Idle(AgentID: Uuid) -> Result<String, std::error::Error> {
    todo!();
}
