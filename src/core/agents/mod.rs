// Standard

// 3rd Party

// Modules
pub mod jobs;

struct Agent {
    // Replace i32 on ID with a UUID type
    ID: i32,
    Platform: String,
    Architecture: String,
    Username: String,
    UserGUID: String,
    HostName: String,
    Integrity: i32,
    Ips: String,
    Pid: i32,
    Process: String,
    agentLog: String, // Replace this string with the rust equivelant of *os.File
    InitialCheckIn: i32, // Replace this with the rust equivelant of time.Time
    StatusCheckIn: i32, // Also replace this i32 with time.Time equivelant
    Version: String,
    Build: String,
    WaitTime: String,
    PaddingMax: i32,
    MaxRetry: i32,
    FailedCheckin: i32,
    Skew: i64,
    Proto: String,
    KillDate: i64,
    RSAKeys: i32, // Replace this with *rsa.PrivateKey equivelant
    PublicKey: i32, // Replace this with rspa.PublicKey equivelant
    Secret: char, // This might be wrong. Was byte in go
    OPAQUE: i32, // Replace with *opaque.Server equivalent
    JA3: String,
    Note: String,
}


