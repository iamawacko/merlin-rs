// Standard
use std::time::Instant;

// 3rd Party

// Modules

const Info: i32 = 0;
const Note: i32 = 1;
const Warn: i32 = 2;
const Debug: i32 = 3;
const Success: i32 = 4;
const Plain: i32 = 5;

struct UserMessage {
    Level: i32,
    Message: String,
    // I might have to change how I do time
    Time: Instant,
    Error: bool,
}

fn SendBroadcastMessage(message: UserMessage) {
    todo!()
}
