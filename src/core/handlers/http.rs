// Standard
use std::time::Instant;

// 3rd Party

// Modules

fn message(level: String, message: String) {
    let m = crate::core::api::messages::UserMessage{
        Message: message,
        // I might have to change how I do Time
        Time: Instant::now(),
        Error: false,
    }

    match level {
        "info" => m.Level = crate::core::api::messages::Info,
        "warn" => m.Level = crate::core::api::messages::Warn,
        "debug" => m.Level = crate::core::api::messages::Debug,
        "success" => m.Level = crate::core::api::messages::Success,
        _=> m.Level = crate::core::api::messages::Plain,
    }
    crate::core::api::messages::SendBroadcastMessage(m)
    todo!()
}
