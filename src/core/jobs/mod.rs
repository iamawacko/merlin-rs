// Standard

// 3rd Party
use uuid::Uuid;

fn init() {
    todo!();
}

const CREATED = 1;
const SENT = 2;
const RETURNED = 3;
const COMPLETE = 4;
const CANCELED = 5;

const CMD = 10;
const CONTROL = 11;
const SHELLCODE = 12;
const NATIVE = 13;
const FILETRANSFER = 14;
const OK = 14;
const MODULE = 16;

const RESULT = 20;
const AGENTINFO = 21;

struct Job {
    AgentID: Uuid,
    ID: String,
    Token: Uuid,
    Type: i32,
    Payload: i32 // Will have to replace i32 with rust equivelant of interface
}

struct Command {
    Command: String,
    Args: String,
}

struct Shellcode {
    Method: String,
    Bytes: String,
    PID: u32,
}

struct FileTransfer {
    FileLocation: String,
    FileBlob: String,
    IsDownload: bool,
}

struct Results {
    Stdout: String,
    Stderr: String,
}

fn String(job_type: i32) -> String {
    match job_type {
        a if a == CMD => "Command",
        b if b == CONTROL => "AgentControl",
        c if c == SHELLCODE => "Shellcode",
        d if d == NATIVE => "Native",
        e if e == FILETRANSFER => "FileTransfer",
        f if f == OK => "ServerOK",
        g if g == MODULE => "Module",
        h if h == RESULT => "Result",
        i if i == AGENTINFO => "AgentInfo",
        _ => format!("Invalid job type: {}", job_type),
    }
}
