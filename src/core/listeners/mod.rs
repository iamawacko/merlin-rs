// Standard

// 3rd Party
use uuid::{Uuid, uuid, Version};

// Modules

struct Listener {
    ID: Uuid,
    Name: String,
    Description: String,
    // I need to replace the string on Server with ServerInterface from servers once I figure out
    // what the rust equivelant of interface{} is
    Server: String,
}


