// Standard

// 3rd Party
use uuid::{uuid, Uuid, Version};

fn init() {
    // Get the GOB binary format working
    todo!()
}

struct Base {
    Version: f32,
    ID: Uuid,
    Type: i32,
    // Figure out what interface is. I have seen it in several structures.
    Payload: interface,
    Padding: String,
    Token: String,
}

struct KeyExchange {
    // Replace this with the rust equivelant of rsa.PublicKey
    PublicKey: rsa::RsaPublicKey,
}

struct SysInfo {
    Platform: String,
    Architecture: String,
    UserName: String,
    UserGUID: String,
    Integrity: i32,
    HostName: String,
    Process: String,
    Pid: i32,
    Ips: String,
    Domain: String,
}

struct AgentInfo {
    Version: String,
    Build: String,
    WaitTime: String,
    PaddingMax: i32,
    MaxRetry: i32,
    FailedCheckin: i32,
    Skew: i64,
    Proto: String,
    SysInfo: SysInfo,
    KillDate: i64,
    JA3: String,
}

fn String(message_type: i32) String {
    match message_type {
        4 => "KeyExchange",
        1 => "StatusCheckIn",
        3 => "Jobs",
        10 => "OPAQUE",
        2 => "Idle",
        _=> format!("Invalid: {}", message_type),
    }
}
