// Standard

// 3rd Party
use uuid::{Uuid, uuid, Version};


struct Module {
    Agent:
    Name: String,
    Author: String,
    Credits: String,
    Path: String,
    Platform: String,
    Arch: String,
    Lang: String,
    Priv: bool,
    Description: String,
    Notes: String,
    Commands: String,
    SourceRemote: String,
    SourceLocal: String,
    Options: Options,
    Powershell: interface,
}

struct Options {
    Name: String,
    Value: String,
    Requied: bool,
    Flag: String,
    Description: String,
}

struct PowerShell {
    DisableAV: bool,
    Obfuscation: bool,
    Base64: bool,
}

fn Run(m: Module) -> Result<(), String> {
    let null_uuid = Uuid::parse_str("00000000-0000-0000-0000-000000000000")?;
    if m.Agent == null_uuid {
        eprintln!("agent not set for module");
        // Reminder to remove the panic in the future
        panic!();
    }

    Ok(())
}
