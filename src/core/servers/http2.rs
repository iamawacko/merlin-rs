// Standard

// 3rd Party

// This part of the code will be the HTTP2 server

struct Server {
    // Figure out what servers.Server is and put it here.
    urls: String,
    // Figure out what *handlers.HTTPContext is and replace the i32 on ctx with it
    ctx: i32,
}

struct Template {
    // Figure out what servers.Template is and put it here
    X509Key: String, // The x.509 private key for TLS
    X509Cert: String, // The x.509 public key for TLS
    PSK: String, // The pre-shared key password used prior to PAKE
}

fn init() {
    todo!()
}

// This will create the HTTP2 server
fn New() -> Result<String, std::error::Error> {
    todo!()
}
