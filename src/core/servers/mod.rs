// This is module is where all of the actual server stuff will go.

// Standard

// 3rd Party

// Modules
pub mod http;
pub mod http2;
pub mod http3;

struct Server {
    // Replace i32 with a UUID type for ID
    ID: i32,
    Transport: interface,
    Interface: String,
    Port: i32,
    Protocol: i32,
    State: i32,
}

struct Template {
    Interface: String,
    Port: String,
    Protocol: String,
}

fn GetProtocol(protocol: i32) -> String {
    // Rust equivelant to switch is match
    const HTTP: i32 = 1;
    const HTTPS: i32 = 2;
    const H2C: i32 = 3;
    const HTTP2: i32 = 4;
    const HTTP3: i32 = 5;
    const DNS: i32 = 6;

    match protocol{
        HTTP => "HTTP",
        HTTPS => "HTTPS",
        H2C => "H2C",
        DNS => "DNS",
        HTTP2 => "HTTP2",
        DNS => "DNS",
        _=> "invalid protocol",
    }
    todo!()
}

fn GetStateString(state: i32) -> String {
    const STOPPED: i32 = 0;
    const RUNNING: i32 = 1;
    const ERROR: i32 = 2;
    const CLOSED: i32 = 3;

    match state{
        STOPPED => "Stopped",
        RUNNING => "Running",
        ERROR => "Error",
        CLOSED => "Closed",
        _ => "Undefined",
    }
}
