// Standard
use std::env;
use std::process::exit;

// 3rd Party
use owo_colors::OwoColorize;

use log::*;
use simplelog::*;

// Modules
pub mod cli;

fn main() -> Result<(), String> {
    let _ = WriteLogger::init(
        LevelFilter::Info,
        Config::default(),
        std::fs::File::create("data/merlinrs.log").unwrap(),
    );

    let build = "Placeholder";
    let version = "Placeholder";

    let flag = env::args().count();

    if flag != 1 {
        eprintln!(
            "{}",
            "#################################################".blue()
        );
        eprintln!("{}", "#\t\tMERLIN SERVER\t\t\t#".blue());
        eprintln!(
            "{}",
            "#################################################".blue()
        );
        eprintln!("{} {}", "Version:".blue(), version.blue());
        eprintln!("{} {}", "Build:".blue(), build.blue());
        eprintln!(
            "{}",
            "Merlin Server does not take any command line arguments".yellow()
        );
        eprintln!("{}", "Visit the Merlin wiki for additional information: https://merlin-c2.readthedocs.io/en/latest/".yellow());
        eprintln!("Exiting");
        error!("Merlin does not take any command line arguments.");
        exit(1);
    }
    info!("Starting Merlin Server version {} build {}", version, build);
    cli::banner::merlin();

    cli::main().expect("REPL failed");

    Ok(())
}
